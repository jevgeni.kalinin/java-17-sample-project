package com.helmes.app;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class DummyTest {

    @Test
    public void test_array_search() {

        int[] subject = new int[] {
                1, 2, 3, 4, 5, 6, 7, 8, 9, 0
        };

        assertTrue(
                Arrays.stream(subject).anyMatch(i -> i == 4)
        );

    }

}
