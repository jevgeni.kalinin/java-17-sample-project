package com.helmes.app;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    public void test_equals() {
        User activeUser = new User(1L, "user@localhost", true);
        User inactiveUser = new User(1L, "user@localhost", false);

        assertTrue(activeUser.equals(inactiveUser));
    }

    @Test
    public void test_not_equals() {
        User a = new User(1l, "user@localhost", true);
        User b = new User(2l, "user@localhost", true);

        assertFalse(a.equals(b));
    }

}